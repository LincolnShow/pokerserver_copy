#include "multyserver.h"

#include <iostream>
#include <string>

int MultyServer::set_nonblock(int fd)
{
    int flags;
#if defined(O_NONBLOCK)
    if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
        flags = 0;
    return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
    flags = 1;
    return ioctl(fd, FIOBIO, &flags);
#endif
}


MultyServer::MultyServer(int masterPort)
{
    std::cout << "Initialisation..." << std::endl;
    MasterSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    tableManager = TableManager::getInstance(this);

    int option = 1;
    setsockopt(MasterSocket, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));

    if(MasterSocket == -1)
    {
        std::cout << "Mastersocket error" << std::endl;
        exit(1);  //return 1
    }
    std::cout << "MasterSocket is "<< MasterSocket << std::endl;
    struct sockaddr_in SockAddr;
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_port = htons(masterPort);
    SockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    std::cout << "Binding is... ";
    int Result = bind(MasterSocket, (struct sockaddr *)&SockAddr, sizeof(SockAddr));

    if(Result == -1)
    {
        std::cout << "binding error" << std::endl;
        exit(1);  //return 1
    }
    std::cout << "OK"<< std::endl;

    set_nonblock(MasterSocket);

    std::cout << "Listening..."<< std::endl;

    Result = listen(MasterSocket, SOMAXCONN);

    if(Result == -1)
    {
        std::cout << "listening error" << std::endl;
        exit(1);//return 1;
    }

    struct epoll_event Event;
    Event.data.fd = MasterSocket;
    Event.events = EPOLLIN | EPOLLET;

    Events = (struct epoll_event *) calloc(MAX_EVENTS, sizeof(struct epoll_event));

    int EPoll = epoll_create1(0);
    epoll_ctl(EPoll, EPOLL_CTL_ADD, MasterSocket, &Event);

    while(true)
    {
        int N = epoll_wait(EPoll, Events, MAX_EVENTS, -1);
        for(int i = 0; i < N; i++)
        {
            currentConnectionOrder = i;
            if((Events[i].events & EPOLLERR)||(Events[i].events & EPOLLHUP))
            {
                shutDownConnection();
            }
            else if(Events[i].data.fd == MasterSocket)
            {
                establishNewConnection(EPoll);
            }
            else
            {
                recieveMessage(Events[i].data.fd);
            }
        }
    }
}

void MultyServer::shutDownConnection()
{
    std::cout << "User socket = "<< Events[currentConnectionOrder].data.fd
              << " disconnected from Server" << std::endl;
    shutdown(Events[currentConnectionOrder].data.fd, SHUT_RDWR);
    auto it=registeredPlayers.find(Events[currentConnectionOrder].data.fd);
    delete it->second;
    registeredPlayers.erase(it);
    close(Events[currentConnectionOrder].data.fd);
}

void MultyServer::establishNewConnection(int EPoll)
{
    int SlaveSocket = accept(MasterSocket, 0, 0);
    set_nonblock(SlaveSocket);

    struct epoll_event Event;
    Event.data.fd = SlaveSocket;
    Event.events = EPOLLIN | EPOLLET;

    epoll_ctl(EPoll, EPOLL_CTL_ADD, SlaveSocket, &Event);

    RegisteredPlayers * newPlayer = new RegisteredPlayers(SlaveSocket, this);
    registeredPlayers.insert(std::pair<int, RegisteredPlayers*>(SlaveSocket, newPlayer));

    std::cout << "New user socket = "<< SlaveSocket
              << " connected to Server" << std::endl;
}

void MultyServer::recieveMessage(int sock)
{
    char Buffer[BUF_SIZE]={0};
    int RecvSize = recv(sock, Buffer, BUF_SIZE, MSG_NOSIGNAL);
    if(Buffer[RecvSize-1]=='\n') Buffer[RecvSize-1]='\0';

    std::cout << "MSG from socket = " << Events->data.fd
              <<" is: " << Buffer << std::endl;
    std::string inString  = std::string(Buffer);
    requestHandle(new Request(inString, (registeredPlayers.find(sock))->second ) );
}


//*******Handling request***********
void MultyServer::requestHandle(Request * inRequest){

    Answer* outAnswer;
    std::map <std::string, unsigned int> tables;
    Dealer* table = nullptr;

    switch (inRequest->type){
    case MSGTYPE::ADMIN:
        switch (inRequest->action) {
        case ACTIONS::LOGIN:
            {
                outAnswer = inRequest->owner->logIn(inRequest);
                outAnswer->recievers.push_back(inRequest->owner);
                if(outAnswer->status == ANSWER_STATUS::SUCCESS)
                    inRequest->inMSG = "A:SUCCESS:login=" + std::to_string(inRequest->owner->getTotalMoney())+"=;";
                sendMessage(outAnswer);
            }
            break;

        case ACTIONS::TAKES_SEAT:
            outAnswer = inRequest->owner->takeSeat(inRequest);
            if(outAnswer->status == ANSWER_STATUS::SUCCESS &&
                    inRequest->owner->activeTable->getStatus() == DEALERSTATUS::WAITING4PLAYERS &&
                    outAnswer->recievers.size() >=2)
            {
                sendMessage(outAnswer); //player takes seat
                outAnswer = inRequest->owner->activeTable->startNewDeal(inRequest);
                sendMessage(outAnswer);
            }
            else
            {
                sendMessage(outAnswer);
            }
            break;
        case ACTIONS::LEAVES_SEAT:
            inRequest->owner->leaveSeat(inRequest);
            break;
        case ACTIONS::GET_TABLES:
            inRequest->inMSG = "A:admin:tablecount="+ std::to_string(tableManager->getNumOfTables())+"=;";
            outAnswer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::ADMIN, inRequest);
            outAnswer->recievers.push_back(inRequest->owner);
            sendMessage(outAnswer); //sending tables count
            tables = tableManager->getTableNames();
            for(auto iter = tables.begin(); iter!= tables.end(); ++iter){
                inRequest->inMSG = "A:" + (*iter).first + ":table=" + std::to_string((*iter).second)+"=;";
                outAnswer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::ADMIN, inRequest);
                outAnswer->recievers.push_back(inRequest->owner);
                sendMessage(outAnswer);
            }
            break;
        case ACTIONS::GET_TABLE_INFO:
            table = tableManager->getTable(inRequest->playerName);
            if(table)
                table->sendTableInfo(inRequest);
            else
                sendMessage(new Answer(ANSWER_STATUS::FAILED, MSGTYPE::ADMIN, inRequest));
            break;
        case ACTIONS::GET_CARDS:
            if(inRequest->owner->activeTable)
                outAnswer = inRequest->owner->activeTable->getPocketCards(inRequest);
                sendMessage(outAnswer);
            break;
        case ACTIONS::INFO:
            inRequest->inMSG = "A:" + inRequest->owner->getNickname()
                    + ":getplayerinfo=" + std::to_string(inRequest->owner->getTotalMoney())
                    + "=;";
            sendMessage(new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::ADMIN, inRequest));
            break;
        default:
            std::cout << "Wrong action type in request! terminating"<< std::endl;
            break;
        }
        break;

    case MSGTYPE::DEALER:
        outAnswer = inRequest->owner->activeTable->deal(inRequest);
        sendMessage(outAnswer);
        break;
    case MSGTYPE::CHAT:        
        sendMessage(inRequest->owner->activeTable->chat(inRequest));
        break;
    default:
        std::cout << "MultyServer::requestHandle: Wrong request type!" << std::endl;
        shutDownConnection();
        break;
    }
}

//******Sending message*****************
void MultyServer::sendMessage(Answer* outAnswer){

    std::cout << "sending message -" << std::endl;
    if(outAnswer->status == ANSWER_STATUS::SUCCESS){
        char Buffer [BUF_SIZE] ={0};
        std::string outMsg = outAnswer->req->inMSG;
        int j=0;
        for (auto iter = outMsg.begin(); iter != outMsg.end(); ++iter)
        {
            Buffer[j] = char(*iter);
            j++;
        }
        Buffer[j] = '\n';
        for (auto iter2 = outAnswer->recievers.begin();
                  iter2 != outAnswer->recievers.end();
                  ++iter2){
            send((*iter2)->getSock(), Buffer, j, MSG_NOSIGNAL);
        }
        if(outAnswer->recievers.empty())
        {
            if(outAnswer->req->owner)
                send(outAnswer->req->owner->getSock(), Buffer, j, MSG_NOSIGNAL);
            else
                std::cout << "no request owner/ msg wasn't send -" << outAnswer->req->inMSG << std::endl;
        }
    }
    else
    {
         char Buffer [BUF_SIZE] = {0};
         std::string outMsg = "FAIL:" + outAnswer->req->inMSG;
         int j=0;
         for (auto iter = outMsg.begin(); iter != outMsg.end(); ++iter)
         {
             Buffer[j] = char(*iter);
             j++;
         }
         Buffer[j] = '\n';
         if(outAnswer->req)
             if(outAnswer->req->owner)
                send((outAnswer->req->owner->getSock()), Buffer, j, MSG_NOSIGNAL);
    }
    delete outAnswer;
}
