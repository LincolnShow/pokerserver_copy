TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    cards.cpp \
    multyserver.cpp \
    tablemanager.cpp \
    registeredplayers.cpp \
    request.cpp \
    table.cpp \
    dealer.cpp \
    compare.cpp

HEADERS += \
    cards.h \
    player.h \
    table.h \
    answer.h \
    deck.h \
    registeredplayers.h \
    multyserver.h \
    config.h \
    tablemanager.h \
    request.h \
    constants.h \
    dealer.h \
    exception.h \
    pot.h \
    compare.h
