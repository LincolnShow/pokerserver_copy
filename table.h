#ifndef TABLE_H
#define TABLE_H

#include "player.h"
#include "answer.h"
#include "tablemanager.h"
#include "constants.h"
#include "request.h"
#include "exception.h"
#include "pot.h"
#include "deck.h"
#include "compare.h"

#include <algorithm>
#include <vector>
#include <list>

class Dealer;
class Pot;
class Deck;

class Table
{
public:
    Table(BLINDS blinds, std::string name);

//    Answer*  Handle(Request *);

    void getPlayersToBeDealt();
    Answer* startingNewHand(Request* inReq);
    Answer* moveButton(Request*);
    Answer* postSB(Request*);
    Answer* postBB(Request*);
    Answer* fold(Request*);
    Answer* raise(Request*);
    Answer* call(Request*);
    Answer* check(Request*);

    Answer* dealFlopCards();
    Answer* dealTurnCard();
    Answer* dealRiverCard();
    std::vector <Answer*> finishDealt();
    Answer* getPossibleActions();
    std::vector <Player*> whoWin(Pot* pot);

    Answer* takeSeat(Request * inReq);
    std::vector<Answer *> leaveSeat(Request* inReq);
    std::vector <Answer*> getDealingPlayers(Request* inReq);
    void cleanFromEmptyPlayers();
    unsigned int getPotSum();

    void shuffle();
    void managePots();
    Answer *removeFromPots(RegisteredPlayers*);
    unsigned int getNumOfActivePlayers();
    unsigned int getNumOfDealingPlayers() {return (*pots.rbegin())->players.size();}
    unsigned int getTotalPlayers();
    bool isSeatBusy(unsigned int);
    void clearHandStory(); // good to protect it with clever clear method
    RegisteredPlayers* getWaitingFor() {return (*waitingFor)->regPlayer;}
    RegisteredPlayers* getLastRaiser() {return (*lastRaiser)->regPlayer;}
    std::vector<Answer *> dealingPreFlopCards();
    Answer* getPocketCards(Request* inReq);
    std::vector<Answer*> getPlayersInfo(Request* inReq);
    unsigned int getBigBlind() {return bigBlind;}
    RegisteredPlayers* nextTurn();
    std::vector <RegisteredPlayers*> getRecievers ();

    void newRound();
protected:

    Deck* deck;
    std::vector <Pot*> pots;
//    std::vector <Request*> HandStory;
    Player* players[6] = {nullptr};
    std::string name;
    bool lastActionIsPostBB;
    unsigned int bigBlind;
    unsigned int button;
//    unsigned int pot = 0;
    std::vector <Player*>::iterator waitingFor;
    std::vector <Player*>::iterator lastRaiser;
    unsigned int tableSize = 6;
    Cards * cards[5] ={nullptr};
    unsigned int lastRaiseSum; //Total: from the beggining the dealt at the preflop faze
    int compare(Player*, Player*);
};

#endif // TABLE_H
