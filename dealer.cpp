#include "dealer.h"

Dealer::Dealer(MultyServer* server, BLINDS blinds, std::string name)
{
    this->server = server;
    table = new Table(blinds, name);
    status = DEALERSTATUS::WAITING4PLAYERS;
    this->name = name;
}

Answer* Dealer::takeSeat(Request* inReq){
    std::cout << "Taking seat number" << inReq->value1 << std::endl;
    if(table->isSeatBusy(inReq->value1) ||
            inReq->value2 > inReq->owner->getTotalMoney())
    {
        return new Answer(ANSWER_STATUS::FAILED, inReq->type, inReq);
    }
    if(status != DEALERSTATUS::WAITING4PLAYERS)
    {
        inReq->inMSG = "D:dealer:pot="
                + std::to_string(table->getPotSum()) + "=;";
        server->sendMessage(new Answer(ANSWER_STATUS::SUCCESS,
                                       MSGTYPE::DEALER, inReq));
        std::vector <Answer*> answers = table->getDealingPlayers(inReq);
        for(auto iter = answers.begin(); iter!= answers.end(); ++iter)
            server->sendMessage(*iter);
        if (status == DEALERSTATUS::DEALING_FLOP ||
                status == DEALERSTATUS::DEALING_TURN ||
                status == DEALERSTATUS::DEALING_RIVER)
        {
            Answer* answer = table->dealFlopCards();
            answer->recievers.clear();
            answer->recievers.push_back(inReq->owner);
            server->sendMessage(answer);
            if(status == DEALERSTATUS::DEALING_TURN ||
                    status == DEALERSTATUS::DEALING_RIVER)
            {
                Answer* answer = table->dealTurnCard();
                answer->recievers.clear();
                answer->recievers.push_back(inReq->owner);
                server->sendMessage(answer);
                if(status == DEALERSTATUS::DEALING_RIVER)
                {
                    Answer* answer = table->dealRiverCard();
                    answer->recievers.clear();
                    answer->recievers.push_back(inReq->owner);
                    server->sendMessage(answer);
                }
            }
        }
    }
    return table->takeSeat(inReq);
}

bool Dealer::leaveSeat(Request* inReq){
    bool answerBool = false;
    std::cout << "leavesSeat" << std::endl;
    std::vector<Answer*> answers = table->leaveSeat(inReq);
    if((*answers.rbegin())->status == ANSWER_STATUS::SUCCESS)
        answerBool = true;
    for (auto iter = answers.begin(); iter!= answers.end(); ++iter)
        server->sendMessage(*iter);
    return answerBool;
}

Answer* Dealer::startNewDeal(Request* inReq){
    Answer* answer = table->startingNewHand(inReq);
    table->cleanFromEmptyPlayers();
    if (answer->status == ANSWER_STATUS::SUCCESS)
    {
        server->sendMessage(answer);    //sending D:dealer:startingnewhand==;
        table->shuffle();
        answer = table->moveButton(inReq);
        server->sendMessage(answer);    //sending D:dealer:isdealer=Num=;
        status = DEALERSTATUS::WAITING4SB;
        inReq->inMSG = "D:dealer:action=postsb=;";
        answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, inReq);
        answer->recievers.push_back(table->getWaitingFor());
    }
    else
    {
        status = DEALERSTATUS::WAITING4PLAYERS;
    }
    return answer;
}

Answer* Dealer::deal(Request *inReq)
{
    std::cout << "dealing..." << std::endl;
    Answer* answer = nullptr;
    if(inReq->owner == table->getWaitingFor()){
        switch (status) {
        case DEALERSTATUS::WAITING4SB:
            answer = table->postSB(inReq);
            if(answer->status == ANSWER_STATUS::SUCCESS)
            {
                server->sendMessage(answer);
                status = DEALERSTATUS::WAITING4BB;
                inReq->inMSG = "D:dealer:action=postbb=;";
                answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, inReq);
                answer->recievers.push_back(table->getWaitingFor());
            }
            return answer;
            break;

        case DEALERSTATUS::WAITING4BB:
            answer = table->postBB(inReq);
            if(answer->status == ANSWER_STATUS::SUCCESS)
            {
                server->sendMessage(answer);
                status = DEALERSTATUS::DEALING_PREFLOP;
                std::vector<Answer*> answers = table->dealingPreFlopCards();
                for(std::vector<Answer*>::iterator iter = answers.begin();
                                                    iter!= answers.end();
                                                     ++iter)
                {
                    server->sendMessage(*iter);

                }
                inReq->inMSG = "D:dealer:action=CR=" + std::to_string(table->getBigBlind()) + ";";
                answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, inReq);
                answer->recievers.push_back(table->getWaitingFor());
            }
            return answer;
            break;

        case DEALERSTATUS::DEALING_PREFLOP:
        case DEALERSTATUS::DEALING_FLOP:
        case DEALERSTATUS::DEALING_TURN:
        case DEALERSTATUS::DEALING_RIVER:
            switch (inReq->action) {
            case ACTIONS::FOLD:
                answer = table->fold(inReq);
                if(answer->status == ANSWER_STATUS::SUCCESS)
                {
                    server->sendMessage(answer);
                    answer = nextTurn();
                }
                return answer;
                break;
            case ACTIONS::RAISE:
                answer = table->raise(inReq);
                if(answer->status == ANSWER_STATUS::SUCCESS)
                {
                    server->sendMessage(answer);
                    answer = nextTurn();
                }
                return answer;
                break;
            case ACTIONS::CALL:
                answer = table->call(inReq);
                if(answer->status == ANSWER_STATUS::SUCCESS)
                {
                    server->sendMessage(answer);
                    answer = nextTurn();
                }
                return answer;
                break;
            case ACTIONS::CHECK:
                answer = table->check(inReq);
                if(answer->status == ANSWER_STATUS::SUCCESS)
                {
                    server->sendMessage(answer);
                    answer = nextTurn();
                }
                return answer;
                break;
            default:
                break;
            }
            break;

        default:
            break;
        }
    }
}

Answer* Dealer::getPocketCards(Request* inReq)
{
    table->getPocketCards(inReq);
}

unsigned int Dealer::getTotalPlayers()
{
    return table->getTotalPlayers();
}

void Dealer::sendTableInfo(Request* inReq){
    std::vector<Answer*> playersINFO;
    playersINFO = table->getPlayersInfo(inReq);
    inReq->inMSG = "A:" + name + ":totalPlayers=" + std::to_string(playersINFO.size())+"=;";
    Answer* answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, inReq);
    answer->recievers.push_back(inReq->owner);
    server->sendMessage(answer);
    for(auto iter=playersINFO.begin(); iter!=playersINFO.end(); ++iter){
        server->sendMessage(*iter);
    }
}

Answer *Dealer::endRound()
{
    Answer* answer;
    if(table->getNumOfDealingPlayers()<2 || status == DEALERSTATUS::DEALING_RIVER)
        answer = finishDealt();
    else
        answer = nextRound();
    return  answer;
}

Answer* Dealer::finishDealt()
{
    Answer* answer;
    std::cout << "Dealer->finishing Dealt"<< std::endl;
    std::vector <Answer*> answers = table->finishDealt();
    for(auto iter = answers.begin(); iter!= answers.end(); ++iter)
        server->sendMessage(*iter);
    table->cleanFromEmptyPlayers();
    if (table->getNumOfActivePlayers() >1)
    {
        status = DEALERSTATUS::STARTING_HAND;
        answer = startNewDeal(new Request("D:dealer:startinghand==;", nullptr));
    }
    else
    {
        Request* req = new Request("A:dealer:leavesseat==;", nullptr);
        req->inMSG = "A:dealer:waitingforplayers==;";
        answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::ADMIN, req);
        answer->recievers = table->getRecievers();
        status = DEALERSTATUS::WAITING4PLAYERS;
    }
    return answer;
}

Answer* Dealer::nextRound()
{
    std::cout << "nextRound.." << std::endl;
    Answer* answer;

    table->managePots();

    switch (status) {
    case DEALERSTATUS::DEALING_PREFLOP:
        status = DEALERSTATUS::DEALING_FLOP;
        answer = table->dealFlopCards();
        break;
    case DEALERSTATUS::DEALING_FLOP:
        status = DEALERSTATUS::DEALING_TURN;
        answer = table->dealTurnCard();
        break;
    case DEALERSTATUS::DEALING_TURN:
        status = DEALERSTATUS::DEALING_RIVER;
        answer = table->dealRiverCard();
        break;
    default:
        std::cout << "Dealer::nextRound: Wrong Dealerstatus!"<< std::endl;
        break;
    }
    server->sendMessage(answer);
    table->newRound();
    answer = table->getPossibleActions();
    return answer;
}

Answer* Dealer::chat(Request* inReq)
{
    Answer* answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::CHAT, inReq);
    answer->recievers = table->getRecievers();
    return answer;
}

Answer* Dealer::nextTurn()
{
    Answer* answer;
    do
    {
        RegisteredPlayers* next = table->nextTurn();
        if (next == table->getLastRaiser() || next == nullptr)
            answer = endRound();
        else
            answer = table->getPossibleActions();
    }
    while (answer->status == ANSWER_STATUS::FAILED
           || (answer->req->action != ACTIONS::STARTINGHAND) &&
                   answer->status == ANSWER_STATUS::FAILED);
    return answer;
}
