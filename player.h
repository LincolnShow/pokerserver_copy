#ifndef PLAYER_H
#define PLAYER_H

#include "cards.h"
#include "constants.h"
#include "exception.h"
#include "registeredplayers.h"
#include <iostream>

class RegisteredPlayers;
class Request;

class Player
{
public:
    RegisteredPlayers * regPlayer = nullptr;
//    bool button;
    bool active;
//active    bool inDealt;
    unsigned int timebank;
    unsigned int seatNumber;
    Cards * pocketCards[2] = {nullptr};
    unsigned int stack;
//    ACTIONS lastAction;
    unsigned int moneyInPot;
    unsigned int getMoney(unsigned int sum){
        if(sum > stack)
        {
            moneyInPot += stack;
            sum = stack;
            stack = 0;
        }
        else
        {
            stack -= sum;
            moneyInPot += sum;
        }
        return sum;
    }

};

#endif // PLAYER_H
