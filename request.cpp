#include "request.h"

///MSG Separators
#define SEPARATOR1 ':'
#define SEPARATOR2 ':'
#define SEPARATOR3 '='
#define SEPARATOR4 '='
#define SEPARATOR5 ';'


/*examples: A:NL200_1:takesseat=3=80; ->A:Dzhei:takesseat=3=80;
*           A:NL200_1:takesseat=3=80;
*           D:Dealer:flop=AhJdTs=;
*           D:Dzhei:raise=20=;
*           D:Dzhei:call==;
*           C:Dzhei:some message==;
*           D:name:getplayerinfo==;
*/
Request::Request(std::__cxx11::string inMsg, RegisteredPlayers * owner)
{
    this->owner = owner;
    this->inMSG = inMsg;
    ///MSG Actions
    const std::string raise       =       "raise";
    const std::string check       =       "check";
    const std::string fold        =       "fold";
    const std::string call        =       "call";
    const std::string postbb      =       "postsbb";
    const std::string postsb      =       "postssb";
    const std::string isDealt     =       "isdealt";
    const std::string sittingOut  =       "sittingout";
    const std::string waiting4BB  =       "waitingbb";
    const std::string earlyBB     =       "earlybb";
    const std::string takesSeat   =       "takesseat";
    const std::string leavesSeat  =       "leavesseat";
    const std::string startingHand=       "startinghand";
    const std::string getTables   =       "gettables";
    const std::string getTableInfo=       "gettableinfo";
    const std::string flop        =       "flop";
    const std::string turn        =       "turn";
    const std::string river       =       "river";
    const std::string shows       =       "shows";
    const std::string login       =       "login";
    const std::string getCards    =       "getpocketcards";
    const std::string getPlayerInfo=      "getplayerinfo";

    std::string tmp;
    unsigned int separator1 = inMsg.find(SEPARATOR1);
    if (separator1 != 1) type = MSGTYPE::ERROR; //throw Exception(std::string("Request: wrong input string"));
    if (separator1 == inMsg.length()) type = MSGTYPE::ERROR;//throw Exception(std::string("Request: wrong input string"));

    unsigned  int separator2 = inMsg.find(SEPARATOR2, separator1+1);
    if (separator2 == std::string::npos) type = MSGTYPE::ERROR;//throw Exception(std::string("Request: wrong input string"));
    if (separator2 >= inMsg.length()) type = MSGTYPE::ERROR;//throw Exception(std::string("Request: wrong input string"));

    unsigned  int separator3 = inMsg.find(SEPARATOR3, separator2+1);
    if (separator3 == std::string::npos) type = MSGTYPE::ERROR;//throw Exception(std::string("Request: wrong input string"));
    if (separator3 >= inMsg.length()) type = MSGTYPE::ERROR;//throw Exception(std::string("Request: wrong input string"));

    unsigned  int separator4 = inMsg.find(SEPARATOR4, separator3+1);
    if (separator4 == std::string::npos) type = MSGTYPE::ERROR;//throw Exception(std::string("Request: wrong input string"));
    if (separator4 >= inMsg.length()) type = MSGTYPE::ERROR;//throw Exception(std::string("Request: wrong input string"));

    unsigned  int separator5 = inMsg.find(SEPARATOR5, separator4+1);
    if (separator5 == std::string::npos) type = MSGTYPE::ERROR;//throw Exception(std::string("Request: wrong input string"));
    if (separator5 > inMsg.length()) type = MSGTYPE::ERROR;//throw Exception(std::string("Request: wrong input string"));

    if (type != MSGTYPE::ERROR)
        switch (inMsg[0]) {
        case 'A':
            type = MSGTYPE::ADMIN;
            break;
        case 'D':
            type = MSGTYPE::DEALER;
            break;
        case 'C':
            type = MSGTYPE::CHAT;
            break;
        default:
            type = MSGTYPE::ERROR;
            break;
        }

    playerName = inMsg.substr(separator1+1, separator2 - separator1-1);

    switch (type){
    case MSGTYPE::CHAT:
        textMessage = inMsg.substr(separator2+1, separator3 - separator2-1);
        value1 = 0;
        value2 = 0;
        msg1.clear();
        msg2.clear();
        action = ACTIONS::NONE;
        break;
    case MSGTYPE::DEALER:
    {
        std::string actionString = inMsg.substr(separator2+1, separator3-separator2-1);
        if      (actionString == raise)         action = ACTIONS::RAISE;
        else if (actionString == check)         action = ACTIONS::CHECK;
        else if (actionString == fold)          action = ACTIONS::FOLD;
        else if (actionString == call)          action = ACTIONS::CALL;
        else if (actionString == postbb)        action = ACTIONS::POSTBB;
        else if (actionString == postsb)        action = ACTIONS::POSTSB;
        else if (actionString == isDealt)       action = ACTIONS::IsDEALT;
        else if (actionString == sittingOut)    action = ACTIONS::SITTINGOUT;
        else if (actionString == waiting4BB)    action = ACTIONS::WAITING4BB;
        else if (actionString == earlyBB)       action = ACTIONS::EARLYBB;
        else if (actionString == startingHand)  action = ACTIONS::STARTINGHAND;
        else if (actionString == flop)          action = ACTIONS::DEALINGFLOP;
        else if (actionString == turn)          action = ACTIONS::DEALINGTURN;
        else if (actionString == river)         action = ACTIONS::DEALINGRIVER;
        else if (actionString == shows)         action = ACTIONS::SHOWS;

        switch (action) {
        case ACTIONS::DEALINGFLOP:
        case ACTIONS::DEALINGTURN:
        case ACTIONS::DEALINGRIVER:
            msg1 = inMsg.substr(separator3+1, separator4 - separator3-1);
            msg2.clear();
            value1 = value2 = 0;
            break;
        case ACTIONS::RAISE:
            msg1 = inMsg.substr(separator3+1, separator4 - separator3-1);
            msg2.clear();
            value1 = std::stoi(msg1);
            value2 = 0;
            break;
        case ACTIONS::IsDEALT:
            msg1 = inMsg.substr(separator3+1, separator4 - separator3-1);
            msg2 = inMsg.substr(separator4+1, separator5 - separator4-1);
            value1 = value2 = 0;
        default:
            msg1.clear();
            msg2.clear();
            value1 = value2 = 0;
            break;
        }

        break;
    }
    case MSGTYPE::ADMIN:
    {
        std::string actionString = inMsg.substr(separator2+1, separator3-separator2-1);
        if      (actionString == takesSeat)     action = ACTIONS::TAKES_SEAT;
        else if (actionString == leavesSeat)    action = ACTIONS::LEAVES_SEAT;
        else if (actionString == login)         action = ACTIONS::LOGIN;
        else if (actionString == getTables)     action = ACTIONS::GET_TABLES;
        else if (actionString == getTableInfo)  action = ACTIONS::GET_TABLE_INFO;
        else if (actionString == getCards)      action = ACTIONS::GET_CARDS;
        else if (actionString == getPlayerInfo) action = ACTIONS::INFO;
        switch (action) {
        case ACTIONS::TAKES_SEAT:
            msg1 = inMsg.substr(separator3+1, separator4 - separator3-1);
            msg2 = inMsg.substr(separator4+1, separator5 - separator4-1);
            value1 = std::stoi(msg1);
            value2 = std::stoi(msg2);
            break;
        case ACTIONS::LOGIN:
        case ACTIONS::LEAVES_SEAT:
            msg1 = inMsg.substr(separator3+1, separator4 - separator3-1);
            msg2 = inMsg.substr(separator4+1, separator5 - separator4-1);
            break;
        case ACTIONS::GET_TABLES:
            break;
        case ACTIONS::GET_TABLE_INFO:
            break;
        case ACTIONS::GET_CARDS:
        case ACTIONS::INFO:
            break;
        default:
            break;
        }

    }
    }
}
