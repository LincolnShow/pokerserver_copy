/*
 *      A0  2   3   4   5   6   7   8   9   T   J   Q   K   A
 *  d   0   4   8   12  16  20  24  28  32  36  40  44  48  52
 *  h   1   5   9   13  17  21  25  29  33  37  41  45  49  53
 *  c   2   6   10  14  18  22  26  30  34  38  42  46  50  54
 *  s   3   7   11  15  19  23  27  31  35  39  43  47  51  55
 *
*/


#include "compare.h"

int ConvertFromPChar(char*From, int*To)
{
    int i=0;
    int j, tmp;
    while(From[i]){
        if(From[i]==' ')
        {
            j=i;
            do{
                From[j]=From[j+1];
                j++;
            }
            while(From[j+1]);
            From[j]=0;
        }
        i++;
    }
    if(i!=14)
        return 0;
    for (i=0; i<7; i++)
    {
        switch (From[i*2])
        {
        case 'A':
            To[i]=Ace;
            break;
        case 'K':
            To[i]=King;
            break;
        case 'Q':
            To[i]=Queen;
            break;
        case 'J':
            To[i]=Jack;
            break;
        case 'T':
            To[i]=Ten;
            break;
        case '9':
            To[i]=Nine;
            break;
        case '8':
            To[i]=Eight;
            break;
        case '7':
            To[i]=Seven;
            break;
        case '6':
            To[i]=Six;
            break;
        case '5':
            To[i]=Five;
            break;
        case '4':
            To[i]=Four;
            break;
        case '3':
            To[i]=Three;
            break;
        case '2':
            To[i]=Duece;
            break;
        default:
            return 0;
        }
        switch (From[i*2+1])
        {
        case 'd':
            To[i]+=Diamond;
            break;
        case 'h':
            To[i]+=Heart;
            break;
        case 'c':
            To[i]+=Clup;
            break;
        case 's':
            To[i]+=Spade;
            break;
        default:
            return 0;
        }
    }
    for(i=0; i<6; i++)
        for(j=i+1; j<7; j++)
        {
            if(To[i]<To[j])
            {
                tmp=To[i];
                To[i]=To[j];
                To[j]=tmp;
            }
        }
    return 1;
}

void ConvertFromPInt(int*From, char*To)
{
    int i;
    for(i=0; i<5; i++)
    {
        switch (From[i]/4) {
        case Ace/4:
            To[i*2]='A';
            break;
        case King/4:
            To[i*2]='K';
            break;
        case Queen/4:
            To[i*2]='Q';
            break;
        case Jack/4:
            To[i*2]='J';
            break;
        case Ten/4:
            To[i*2]='T';
            break;
        case Nine/4:
            To[i*2]='9';
            break;
        case Eight/4:
            To[i*2]='8';
            break;
        case Seven/4:
            To[i*2]='7';
            break;
        case Six/4:
            To[i*2]='6';
            break;
        case Five/4:
            To[i*2]='5';
            break;
        case Four/4:
            To[i*2]='4';
            break;
        case Three/4:
            To[i*2]='3';
            break;
        case Duece/4:
            To[i*2]='2';
            break;
        default:
            break;
        }
        switch (From[i]%4) {
        case Diamond:
            To[i*2+1]='d';
            break;
        case Heart:
            To[i*2+1]='h';
            break;
        case Clup:
            To[i*2+1]='c';
            break;
        case Spade:
            To[i*2+1]='s';
            break;
        default:
            break;
        }
    }
    To[10]=0;
}

int Compare(int *Hand1, int *Hand2)
{
    int Combination1, Combination2;

    //straight flush
    if (StraightFlush(Hand1))
        if (StraightFlush(Hand2))
            if(Hand1[0]>Hand2[0])
                return 1;
            else if (Hand1[0]<Hand2[0])
                return 2;
            else
                return 0;
        else
            return 1;
    else if (StraightFlush(Hand2))
        return 2;

    //Kare
    if (Kare(Hand1))
        if (Kare(Hand2))
            if(Hand1[0]>Hand2[0])
                return 1;
            else if (Hand1[0]<Hand2[0])
                return 2;
            else if (Hand1[4]>Hand2[4])
                return 1;
            else if (Hand2[4]>Hand1[4])
                return 2;
            else
                return 0;
        else
            return 1;
    else if (Kare(Hand2))
        return 2;

    //FullHouse
    if (FullHouse(Hand1))
    {
        if(FullHouse(Hand2))
        {
            if(Hand1[0]>Hand2[0]) return 1;
            if(Hand1[0]<Hand2[0]) return 2;
            if(Hand1[3]>Hand2[3]) return 1;
            if(Hand1[3]<Hand2[3]) return 2;
            return 0;
        }
        else return 1;
    }
    if (FullHouse(Hand2)) return 2;

    //Flush
    if (Flush(Hand1))
    {
        if(Flush(Hand2))
        {
            if(Hand1[0]>Hand2[0]) return 1;
            if(Hand1[0]<Hand2[0]) return 2;
            if(Hand1[1]>Hand2[1]) return 1;
            if(Hand1[1]<Hand2[1]) return 2;
            if(Hand1[2]>Hand2[2]) return 1;
            if(Hand1[2]<Hand2[2]) return 2;
            if(Hand1[3]>Hand2[3]) return 1;
            if(Hand1[3]<Hand2[3]) return 2;
            if(Hand1[4]>Hand2[4]) return 1;
            if(Hand1[4]<Hand2[4]) return 2;
            return 0;
        }
        return 1;
    }
    if(Flush(Hand2)) return 2;

    //Straight
    if (Straight(Hand1))
    {
        if(Straight(Hand2))
        {
            if(Hand1[0]>Hand2[0]) return 1;
            if(Hand1[0]<Hand2[0]) return 2;
            return 0;
        }
        return 1;
    }
    if(Straight(Hand2)) return 2;

    //pair-trips
    Combination1=getPared(Hand1);
    Combination2=getPared(Hand2);

    if(Combination1==TRIPS)
    {
        if(Combination2==TRIPS)
        {
            if(Hand1[0]>Hand2[0]) return 1;
            if(Hand1[0]<Hand2[0]) return 2;
            if(Hand1[3]>Hand2[3]) return 1;
            if(Hand1[3]<Hand2[3]) return 2;
            if(Hand1[4]>Hand2[4]) return 1;
            if(Hand1[4]<Hand2[4]) return 2;
            return 0;
        }
        else return 1;
    }
    if (Combination2==TRIPS) return 2;

    if(Combination1==TWOPAIRS)
    {
        if(Combination2==TWOPAIRS)
        {
            if(Hand1[0]>Hand2[0]) return 1;
            if(Hand1[0]<Hand2[0]) return 2;
            if(Hand1[2]>Hand2[2]) return 1;
            if(Hand1[2]<Hand2[2]) return 2;
            if(Hand1[4]>Hand2[4]) return 1;
            if(Hand1[4]<Hand2[4]) return 2;
            return 0;
        }
        else return 1;
    }
    if (Combination2==TWOPAIRS) return 2;

    if(Combination1==PAIR)
    {
        if(Combination2==PAIR)
        {
            if(Hand1[0]>Hand2[0]) return 1;
            if(Hand1[0]<Hand2[0]) return 2;
            if(Hand1[2]>Hand2[2]) return 1;
            if(Hand1[2]<Hand2[2]) return 2;
            if(Hand1[3]>Hand2[3]) return 1;
            if(Hand1[3]<Hand2[3]) return 2;
            if(Hand1[4]>Hand2[4]) return 1;
            if(Hand1[4]<Hand2[4]) return 2;
            return 0;
        }
        else return 1;
    }
    if (Combination2==PAIR) return 2;

    if(Hand1[0]>Hand2[0]) return 1;
    if(Hand1[0]<Hand2[0]) return 2;
    if(Hand1[1]>Hand2[1]) return 1;
    if(Hand1[1]<Hand2[1]) return 2;
    if(Hand1[2]>Hand2[2]) return 1;
    if(Hand1[2]<Hand2[2]) return 2;
    if(Hand1[3]>Hand2[3]) return 1;
    if(Hand1[3]<Hand2[3]) return 2;
    if(Hand1[4]>Hand2[4]) return 1;
    if(Hand1[4]<Hand2[4]) return 2;
    return -1;
}


int StraightFlush   (int *Hand)             //if true, first 5 in Hand is the Best combo
{
    int i, j, k=0;
    int FL[4]={0};
    int Combs[7];
    for(i=0; i<7; i++)
        FL[Hand[i]%4]++;
    for(i=0; i<7; i++)
    {
        if(FL[i]>4)
        {
            for(j=0; j<7; j++)
            {
                if(Hand[j]%4==i)
                {
                    Combs[k]=Hand[j];
                    k++;
                }
            }
            for(j=k; j<7; j++)
                Combs[j]=Combs[k-1];
            if(Straight(Combs))
            {
                Hand[0]=Combs[0];
                Hand[1]=Combs[1];
                Hand[2]=Combs[2];
                Hand[3]=Combs[3];
                Hand[4]=Combs[4];
                return 1;
            }
            return 0;
        }
    }
    return 0;
}

int Kare (int *Hand)
{
    int i, j, k;
    for(i=0; i<4; i++)
    {
        k=1;
        j=i+1;
        while(Hand[i]/4==Hand[j]/4){
            k++;
            j++;
        }
        if(k==4)
        {
            if(i!=0)
            {
                j=Hand[0];
                Hand[0]=Hand[i];
                Hand[1]=Hand[i+1];
                Hand[2]=Hand[i+2];
                Hand[3]=Hand[i+3];
                Hand[4]=j;
                return 1;
            }
            else
                return 1;
        }
        i+=(k-1);
    }
    return 0;
}

int FullHouse       (int *Hand)
{
    int Combs[14]={0};
    int i;
    int trips=0;
    int pair1=0;

    for(i=0; i<7; i++)
        Combs[Hand[i]/4]++;
    for(i=13; i>=0; i--)
    {
        if(Combs[i]==3 && trips==0)
        {
            trips=i;
            if(pair1>0)
                break;
        }
        if(Combs[i]==2)
        {
            if(pair1==0)
            {
                pair1=i;
                if(trips>0)
                    break;
            }
        }
    }
    i=0;
    //Full
    if(trips>0 && pair1>0)
    {
        while(Hand[i]/4!=trips)
            i++;
        Combs[0]=Hand[i];
        Combs[1]=Hand[i+1];
        Combs[2]=Hand[i+2];
        i=0;
        while (Hand[i]/4!=pair1)
            i++;
        Combs[3]=Hand[i];
        Combs[4]=Hand[i+1];

        for(i=0;i<5;i++)
            Hand[i]=Combs[i];
        return 1;
    }
    return 0;
}

int getPared      (int *Hand)
{
    int Combs[14]={0};
    int i, j=0;
    int trips=0;
    int pair1=0;
    int pair2=0;

    for(i=0; i<7; i++)
        Combs[Hand[i]/4]++;
    for(i=13; i>=0; i--)
    {
        if(Combs[i]==3 && trips==0)
        {
            trips=i;
            break;
        }
        if(Combs[i]==2)
        {
            if(pair1==0)
            {
                pair1=i;
                if(trips>0)
                    break;
            }
            else if (pair2==0)
                pair2=i;
        }
    }

    //Trips
    if(trips>0 && pair1==0)
    {
        i=0;
        while(Hand[i]/4!=trips){
            Combs[i+3]=Hand[i];
            i++;
        }
        if(i>0)
        {
            Combs[0]=Hand[i];
            Combs[1]=Hand[i+1];
            Combs[2]=Hand[i+2];
            for(i=0; i<5; i++)
                Hand[i]=Combs[i];
        }
        return TRIPS;
    }
    if(pair1>0)
    {
        //2pairs
        if(pair2>0)
        {
            i=0;
            while(Hand[i]/4!=pair1){
                i++;
            }
            if(i>0)
            {
                Combs[4]=Hand[0];
                Hand[0]=Hand[i];
                Hand[1]=Hand[i+1];
            }
            i+=2;
            j=i;
            while(Hand[i]/4!=pair2){
                i++;
            }
            if(i>2)
            {
                Combs[2]=Hand[i];
                Combs[3]=Hand[i+1];
                if(Combs[4]<8)
                    Combs[4]=Hand[2];
                for(i=2; i<5; i++)
                    Hand[i]=Combs[i];
            }
            return TWOPAIRS;
        }
        else
        {
            i=0;
            while(Hand[i]/4!=pair1){
                Combs[i+2]=Hand[i];
                i++;
            }
            if(i>0)
            {
                Combs[0]=Hand[i];
                Combs[1]=Hand[i+1];
                i+=2;
                if(i<5)
                    for(;i<5;i++)
                        Combs[i]=Hand[i];
                for(i=0; i<5; i++)
                    Hand[i]=Combs[i];
            }
            return PAIR;
        }
    }
    return 0;
}

int Flush (int *Hand)
{
    int i, j, k=0;
    int Comb[4]={0};
    for(i=0;i<7;i++)
        Comb[Hand[i]%4]++;
    for(i=0;i<4;i++)
        if(Comb[i]>4)
        {
            for(j=0; j<7;j++)
                if(Hand[j]%4==i)
                {
                    Hand[k]=Hand[j];
                    k++;
                    if(k==5) return 1;
                }
        }
    return 0;
}

int Straight        (int *Hand)
{
    int i, j, k=0;
    int Combs[5]={0};
    for(i=0; i<4; i++)
    {
        Combs[0]=Hand[i];
        j=i+1;
        k=0;
        while(j<7)
        {
            if(Hand[j]/4+1==Combs[k]/4)
            {
                k++;
                Combs[k]=Hand[j];
            }
            if(k==4)
            {
                for(k=0; k<5; k++)
                    Hand[k]=Combs[k];
                return 1;
            }
            if(k==3 && Combs[k]/4==1 && Hand[0]/4==13)
            {
                Combs[k+1]=Hand[0];
                for(k=0; k<5; k++)
                    Hand[k]=Combs[k];
                return 1;
            }
            j++;
        }
    }
    return 0;
}
