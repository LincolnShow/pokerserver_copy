#ifndef DECK_H
#define DECK_H

#include "cards.h"
#include "exception.h"
#include "dealer.h"
#include <algorithm>
#include <vector>
#include <random>
#include <chrono>

class Dealer;
class Cards;

class Deck
{
public:
    Deck(){for(int i=0; i<52; i++) deck.push_back(new Cards(i));}
    ~Deck(){}
    void shuffle() {std::shuffle(deck.begin(), deck.end(),
     std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count()));
    }
    std::vector <Cards*> deck;
//    unsigned int currentCardIndex = 0;
};

#endif // DECK_H
