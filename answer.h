#ifndef ANSWER_H
#define ANSWER_H

#include <string>
#include <vector>
#include <iostream>
#include <request.h>

#include "constants.h"
#include "exception.h"

class Request;
class Dealer;
class RegisteredPlayers;

class Answer
{
public:
    Answer(){}
    Answer(ANSWER_STATUS status, MSGTYPE type, Request* req){
        this->status = status;
        msgType = type;
        this->req = req;
    }
    ~Answer(){
//        delete req;
    }

    Request* req;
//    Dealer* table;
//    std::string toString();
    ANSWER_STATUS status;
    MSGTYPE msgType;
//    std::vector <Request*> currentRequestStory;
    std::vector <RegisteredPlayers*> recievers;
//    std::vector <RegisteredPlayers*> actioners;
};

#endif // ANSWER_H
