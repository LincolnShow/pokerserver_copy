#ifndef DEALER_H
#define DEALER_H

#include "table.h"
#include "deck.h"
#include "request.h"
#include "answer.h"
#include "exception.h"
#include "multyserver.h"

class Answer;
class Table;
class Request;
class MultyServer;

class Dealer
{
public:
    Dealer(MultyServer* server, BLINDS, std::string name);
    Answer* takeSeat(Request* inReq);
    bool leaveSeat(Request* inReq);
    Answer* startNewDeal(Request* inReq);
    Answer* deal(Request* inReq);
    Answer* endRound();
    Answer* finishDealt();
    Answer* nextRound();
    Answer* getPocketCards(Request* inReq);
    Answer* chat(Request* inReq);

    DEALERSTATUS getStatus(){return status;}    
    unsigned int getTotalPlayers();
    void sendTableInfo(Request* inReq);
    bool isEmpty(){return empty;}
    std::string getName(){return name;}

protected:
    MultyServer* server;
    DEALERSTATUS status;
    Table * table;
    BLINDS bigBlind;
    Answer* nextTurn();


    //TableManager
    bool empty;
    std::string name;
};

#endif // DEALER_H
