#ifndef MULTYSERVER_H
#define MULTYSERVER_H

#include <iostream>
#include <algorithm>
#include <set>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>

#include <sys/epoll.h>

#include <errno.h>
#include <string>

#include <map>

#include "config.h"
#include "registeredplayers.h"
#include "tablemanager.h"
#include "exception.h"
#include "request.h"

class RegisteredPlayers;
class TableManager;

/**
 * @brief Initialises Network Connection, creates Tables and executes its
 *
 */
class MultyServer
{
protected:
    struct epoll_event * Events;
    int MasterSocket;
    std::map <int, RegisteredPlayers *> registeredPlayers;
    TableManager* tableManager;
    unsigned int currentConnectionOrder;


    /**
     * @brief Unblocks Socket
     * @param fd - Socket descriptor
     * @return Returns -1 if error
     */
    int set_nonblock(int fd);

    /**
     * @brief Recievs/Sends messages and executes nessesory User's object with incoming string as a parametr.
     * @param fd - Socket descriptor
     * @return Returns -1 if error
     */
    void recieveMessage(int sock);

    void requestHandle(Request *);

    void shutDownConnection();

    void establishNewConnection(int);

public:
    /**
     * @brief MultyServer constructor. Initialises connection with clients. Waits for incoming message.
     * @param masterPort - Listening port
     *
     */
    MultyServer(int masterPort);
    /**
     * @brief Sends messages to clients. Knows how to send class Answer messages
     * @param class Answer
     */

    void sendMessage(Answer *);
};

#endif // MULTYSERVER_H
