/*
 *      A0  2   3   4   5   6   7   8   9   T   J   Q   K   A
 *  d   0   4   8   12  16  20  24  28  32  36  40  44  48  52
 *  h   1   5   9   13  17  21  25  29  33  37  41  45  49  53
 *  c   2   6   10  14  18  22  26  30  34  38  42  46  50  54
 *  s   3   7   11  15  19  23  27  31  35  39  43  47  51  55
 *
 *
 * gets Hand as int[7] "55 46 32 25 22 9 8 0". Only backArranged.
 * returns 0 if equal, else number of winning hand.
*/

#ifndef COMPARE_H
#define COMPARE_H


#define Ace 52
#define King 48
#define Queen 44
#define Jack 40
#define Ten 36
#define Nine 32
#define Eight 28
#define Seven 24
#define Six 20
#define Five 16
#define Four 12
#define Three 8
#define Duece 4

#define Diamond 0
#define Heart 1
#define Clup 2
#define Spade 3

#define KARE 5
#define FULLHOUSE 4
#define TRIPS 3
#define TWOPAIRS 2
#define PAIR 1

int ConvertFromPChar(char*From, int*To);
void ConvertFromPInt(int*From, char*To);

int Compare (int *Hand1, int *Hand2);
int StraightFlush   (int *Hand);             //if true, first 5 in Hand is the Best combo
int Kare            (int *Hand);
int FullHouse       (int *Hand);
int getPared        (int *Hand);
int Flush           (int *Hand);
int Straight        (int *Hand);

#endif // COMPARE_H
