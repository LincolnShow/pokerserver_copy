#ifndef REGISTEREDPLAYERS_H
#define REGISTEREDPLAYERS_H

#include <map>
#include <string>
#include <fstream>

#include "answer.h"
#include "request.h"
#include "exception.h"
#include "dealer.h"
#include "tablemanager.h"
#include "multyserver.h"

class Answer;
class Request;
class Dealer;
class TableManager;
class MultyServer;

class RegisteredPlayers
{
public:
    RegisteredPlayers(int sock, MultyServer* server);
    ~RegisteredPlayers();

    Answer* logIn(Request* inRequest);

    Dealer* activeTable;

    bool isLoggedIn(){
        return loggedIn;
    }

    int getTotalMoney(){
        return freeMoney;
    }

    void putMoney(unsigned int sum){
        freeMoney += sum;
    }

    bool getMoney(unsigned int summ){
        if(summ <= freeMoney){
            freeMoney -= summ;
            return true;
        }
        return false;
    }

    std::string getNickname(){
        return nickname;
    }

    Answer* takeSeat(Request * inRequest);
    void leaveSeat(Request * inRequest);

    int getSock(){
        return sockfd;
    }

protected:
    bool loggedIn;
    unsigned int freeMoney;
    std::string nickname;
    int sockfd;
    const std::string filePath = "passwords";
    TableManager* tableManager;

};

#endif // REGISTEREDPLAYERS_H
