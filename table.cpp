       #include "table.h"

Table::Table (BLINDS blinds, std::__cxx11::string name):
    name(name)
{
    switch (blinds) {
    case BLINDS::NL200:
        bigBlind = 2;
        break;
    case BLINDS::NL400:
        bigBlind = 4;
        break;
    case BLINDS::NL1000:
        bigBlind = 10;
        break;
    default:
        break;
    }
    button = 7;
    deck = new Deck;
    deck->shuffle();
}

Answer* Table::takeSeat(Request *request)
{
    std::cout << "Player " << request->owner->getNickname()
              << " takes seat number " << request->value1
              << " with " << request->value2 << " chips"
              << std::endl;
    Player* player = new Player();
    player->active = true;
    player->regPlayer = request->owner;
    player->seatNumber = request->value1;
    player->stack = request->value2;
    request->owner->getMoney(player->stack);
    players[request->value1] = player;
    request->inMSG = "A:"
            + request->owner->getNickname()
            + ":takesseat=" + std::to_string(request->value1)
            + "=" + std::to_string(request->value2) + ";";
    Answer* answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::ADMIN, request);
    for (int i=0; i<tableSize; i++){
        if(players[i])
            answer->recievers.push_back(players[i]->regPlayer);
    }
    return answer;
}

std::vector<Answer*> Table::leaveSeat(Request* inReq)
{
    std::vector <Answer*> answers;
    unsigned int seat = tableSize;
    for(unsigned int i = 0; i<tableSize; i++)
        if(players[i])
            if(players[i]->regPlayer == inReq->owner)
                seat = i;
    if(seat<tableSize)
    {
        players[seat]->regPlayer->putMoney(players[seat]->stack);
        Player* dPlayer = players[seat];
        Answer* answer = removeFromPots(dPlayer->regPlayer);

        if(answer)
            answers.push_back(answer);
        inReq->inMSG = "A:" + inReq->playerName+":leavesseat="+std::to_string(seat)+"=;";
        answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::ADMIN, inReq);
        answers.insert(answers.begin(), answer);
        for (auto iter = answers.begin(); iter!= answers.end(); ++iter)
            (*iter)->recievers = getRecievers();

        players[seat] = nullptr;
        delete dPlayer;
        return answers;
    }
    answers.push_back(new Answer(ANSWER_STATUS::FAILED, MSGTYPE::ADMIN, inReq));
    return answers;
}

Answer* Table::startingNewHand(Request* inReq)
{
    inReq->inMSG = "D:dealer:startingnewhand==;";
    Pot* pot = new Pot();
    pots.clear();
    pots.push_back(pot);
    std::vector <Player*> tmpPlayers;
    if(button == 7) {           //Starting Hand
        for(int i=0; i<tableSize; i++){
            if(players[i])
                if(players[i]->stack > bigBlind)
                    tmpPlayers.push_back(players[i]);
        }
    }
    else{                       //else moving button
        for(int i=button+1; i<tableSize; i++){
            if(players[i])
                if(players[i]->stack > bigBlind)
                    tmpPlayers.push_back(players[i]);
        }
        for(int i=0; i<=button; i++){
            if(players[i])
                if(players[i]->stack > bigBlind)
                    tmpPlayers.push_back(players[i]);
        }
    }
    for (auto iter = tmpPlayers.begin(); iter != tmpPlayers.end(); ++iter)
        (*iter)->moneyInPot = 0;
    Answer *answer = new Answer(ANSWER_STATUS::FAILED, MSGTYPE::DEALER, inReq);;
    if(tmpPlayers.size() > 1)
    {
        pot->players = tmpPlayers;
        answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, inReq);
    }
    else
    {
        delete pot;
        pots.clear();
        inReq->inMSG = "A:dealer:waitingforplayers==;";
        Answer *answer = new Answer(ANSWER_STATUS::FAILED, MSGTYPE::DEALER, inReq);
    }
    for (unsigned int i=0; i<tableSize; i++){
        if(players[i])
            answer->recievers.push_back(players[i]->regPlayer);
    }
    return answer;
}

Answer* Table::moveButton(Request * inReq)
{
    waitingFor = (*(pots.cbegin()))->players.begin();
    button = (*waitingFor)->seatNumber;
    if((*(pots.cbegin()))->players.size() > 2){
        ++waitingFor;
    }
    inReq->inMSG = std::string("D:dealer:isdealer=") + std::to_string(button) + "=;";
    inReq->type = MSGTYPE::DEALER;
    Answer *answer = new Answer(ANSWER_STATUS::SUCCESS, inReq->type, inReq);
    for (int i=0; i<tableSize; i++){
        if(players[i])
            answer->recievers.push_back(players[i]->regPlayer);
    }
    return answer;
}

bool Table::isSeatBusy(unsigned int num)
{
    if(players[num])
        return true;
    return false;
}

Answer* Table::postSB(Request* inReq)
{
    if((*waitingFor)->stack >= bigBlind/2)
    {
        (*waitingFor)->stack -= bigBlind/2;
        (*waitingFor)->moneyInPot += bigBlind/2;
        Pot* pot = *(pots.cbegin());
        pot->sum += bigBlind/2;
        lastRaiser = waitingFor;
        ++waitingFor;
        lastRaiseSum = bigBlind/2;
        Answer *answer = new Answer(ANSWER_STATUS::SUCCESS, inReq->type, inReq);
        for (int i=0; i<tableSize; i++){
            if(players[i])
                answer->recievers.push_back(players[i]->regPlayer);
        }
        return answer;
    }
    return new Answer(ANSWER_STATUS::FAILED, inReq->type, inReq);
}

Answer* Table::postBB(Request* inReq)
{
    Answer* answer = new Answer(ANSWER_STATUS::FAILED, inReq->type, inReq);
    if((*waitingFor)->stack >= bigBlind){
        Pot* currentPot = *(pots.rbegin());
        (*waitingFor)->stack -= bigBlind;
        (*waitingFor)->moneyInPot += bigBlind;
        currentPot->sum += bigBlind;
        lastRaiser = waitingFor; //means that bb has the last word
        lastRaiseSum = bigBlind;
        lastActionIsPostBB = true;
        nextTurn();
        answer->status = ANSWER_STATUS::SUCCESS;
        for (int i=0; i<tableSize; i++){
            if(players[i])
                answer->recievers.push_back(players[i]->regPlayer);
        }
        return answer;
    }
    return answer;
}

std::vector<Answer*> Table::dealingPreFlopCards()
{
    std::vector<Answer*> answers;
    Request *request;
    Answer * answer;
    int i = 0;
    std::vector<Player*>* playersDealt = &((*pots.rbegin())->players);
    for(auto iter = playersDealt->begin();
             iter!= playersDealt->end();
             ++iter){
        (*iter)->pocketCards[0] = deck->deck[6+i];
        (*iter)->pocketCards[1] = deck->deck[6+i+1];
        i += 2;
        request = new Request("D:dealer:isdealt==;", nullptr);
        request->inMSG = "D:dealer:isdealt=" + std::to_string((*iter)->seatNumber) + "=;";
        answer = new Answer(ANSWER_STATUS::SUCCESS, request->type, request);
        for (int i=0; i<tableSize; i++){
            if(players[i])
                answer->recievers.push_back(players[i]->regPlayer);
        }
        answers.push_back(answer);
    }
    return answers;
}

unsigned int Table::getNumOfActivePlayers(){
    unsigned int num = 0;
    for (int i = 0; i < 6; i++){
        if(players[i])
            if(players[i]->active)
                num++;
    }
    return num;
}

void Table::shuffle()
{
    deck->shuffle();
}

Answer* Table::getPocketCards(Request* inReq)
{
    for(int i = 0; i<tableSize; i++){
        if(players[i])
            if(players[i]->regPlayer == inReq->owner)
            {
                if(players[i]->pocketCards[0]){
                    std::string outStr = "A:dealer:isdealt=";
                    outStr += players[i]->pocketCards[0]->toString();
                    outStr += "=";
                    outStr += players[i]->pocketCards[1]->toString();
                    outStr += ";";
                    inReq->inMSG = outStr;
                    Answer* outAnswer = new Answer(ANSWER_STATUS::SUCCESS, inReq->type, inReq);
                    outAnswer->recievers.push_back(inReq->owner);
                    return outAnswer;
                }
            }
    }
    return new Answer(ANSWER_STATUS::FAILED, inReq->type, inReq);
}

Answer* Table::fold(Request* inReq)
{
    std::vector <Player*>::iterator folderIt = waitingFor;
    Player* lRaiser = *lastRaiser;
    Player* beforeFolderPl;
    if(folderIt == (*pots.rbegin())->players.begin())
        beforeFolderPl = *((*pots.rbegin())->players.rbegin());
    else
        beforeFolderPl = *(--waitingFor);
    bool success = false;

    if (folderIt == lastRaiser)
    {
        lRaiser = *(++lastRaiser);
        if (lastRaiser == (*pots.rbegin())->players.end())
        {
            lastRaiser = (*pots.rbegin())->players.begin();
            lRaiser = *(lastRaiser);
        }
    }

    //delete from all pots and playersDealt.
    for(auto iter = pots.begin();
             iter!= pots.end(); ++iter){
        std::vector<Player*>::iterator fPlayer = std::find((*iter)->players.begin(),
                                                           (*iter)->players.end(), *folderIt);
        if(fPlayer != (*iter)->players.end()){
            (*iter)->players.erase(fPlayer);
            success = true;
        }
    }
    if(success)
    {
        Pot* cPot = *pots.rbegin();
        for (auto iter = cPot->players.begin();
                  iter!= cPot->players.end(); ++iter)
        {
            if((*iter) == lRaiser)
                lastRaiser = iter;
            if((*iter) == beforeFolderPl)
                waitingFor = iter;
        }
        Answer* answer = new Answer(ANSWER_STATUS::SUCCESS, inReq->type, inReq);
        for(int i = 0; i<tableSize; i++)
            if(players[i])
                answer->recievers.push_back(players[i]->regPlayer);
        return answer;
    }
    return new Answer(ANSWER_STATUS::FAILED, inReq->type, inReq);
}

Answer* Table::raise(Request* inReq)
{
    Answer* answer;
    if((*waitingFor)->getMoney(inReq->value1))
    {
            (*pots.rbegin())->sum += inReq->value1;
            lastRaiser = waitingFor;
            lastRaiseSum = inReq->value1 + (*lastRaiser)->moneyInPot;
            lastActionIsPostBB = false;
            answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, inReq);
            for(int i = 0; i<tableSize; i++)
                if(players[i])
                    answer->recievers.push_back(players[i]->regPlayer);
            return answer;
    }
    return new Answer(ANSWER_STATUS::FAILED, MSGTYPE::DEALER, inReq);
}

Answer* Table::call(Request* inReq)
{
    Answer* answer;
    unsigned int sum;
    if(sum = (*waitingFor)->getMoney
            ((*lastRaiser)->moneyInPot - (*waitingFor)->moneyInPot))
    {
        (*pots.rbegin())->sum += sum;
        if(lastActionIsPostBB)
        {
            lastActionIsPostBB = false;
            lastRaiser = waitingFor;
        }
        inReq->inMSG = "D:" +
                        inReq->playerName
                        + ":call="
                        + std::to_string(sum)
                        + "=;";
        answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, inReq);
        for(int i = 0; i<tableSize; i++)
            if(players[i])
                answer->recievers.push_back(players[i]->regPlayer);
        return answer;
    }
    return new Answer(ANSWER_STATUS::FAILED, MSGTYPE::DEALER, inReq);
}

Answer* Table::check(Request* inReq)
{
    Answer * answer;
    if((*lastRaiser)->moneyInPot == (*waitingFor)->moneyInPot)
    {
         answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, inReq);
         answer->recievers = getRecievers();
    }
    else
        answer = new Answer(ANSWER_STATUS::FAILED, MSGTYPE::DEALER, inReq);
    return answer;
}

unsigned int Table::getTotalPlayers()
{
    unsigned int N = 0;
    for(int i=0; i<tableSize; i++)
        if(players[i] != nullptr) N++;
    return N;
}

std::vector<Answer*> Table::getPlayersInfo(Request* inReq)
{
    std::vector<Answer*> playersInfo;
    Answer* outAnswer;
    for(int i=0; i<tableSize; i++){
        if(players[i])
        {
            Request *req = new Request();
            req->inMSG = "A:"+players[i]->regPlayer->getNickname()+":";
            if(players[i]->active)
                req->inMSG +="active=";
            else
                req->inMSG +="passive=";
            req->inMSG += std::to_string(i) + "=" + std::to_string(players[i]->stack) + ";";
            outAnswer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, req);
            outAnswer->recievers.push_back(inReq->owner);
            playersInfo.push_back(outAnswer);
        }
    }
    return playersInfo;
}

RegisteredPlayers* Table::nextTurn()
{
    std::cout << "calculating nextTurn" << std::endl;
    if (pots.size() < 0)
    {
        std::cout << "no pots!" <<std::endl;
        return nullptr;
    }
    if((*pots.rbegin())->players.size() < 2)
    {
        std::cout << "players less than 2" << std::endl;
        lastRaiser = (*pots.rbegin())->players.begin();
        waitingFor = (*pots.rbegin())->players.begin();
        return (*waitingFor)->regPlayer;
    }
    ++waitingFor;
    if(waitingFor == (*pots.rbegin())->players.end())
        waitingFor = (*pots.rbegin())->players.begin();
    return (*waitingFor)->regPlayer;
}

Answer* Table::dealFlopCards()
{
    cards[0] = deck->deck[0];
    cards[1] = deck->deck[1];
    cards[2] = deck->deck[2];
    std::string str = "D:dealer=" + cards[0]->toString()
                                    + cards[1]->toString()
                                    + cards[2]->toString()
                                    + "=;";
    Request* req = new Request(str, getWaitingFor());
    Answer* answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, req);
    for (int i=0; i<tableSize; i++){
        if(players[i])
            answer->recievers.push_back(players[i]->regPlayer);
    }
    return answer;
}

Answer* Table::dealTurnCard()
{
    cards[3] = deck->deck[3];
    std::string str = "D:dealer=" + cards[3]->toString()
                                    + "=;";
    Request* req = new Request(str, getWaitingFor());
    Answer* answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, req);
    for (int i=0; i<tableSize; i++){
        if(players[i])
            answer->recievers.push_back(players[i]->regPlayer);
    }
    return answer;
}

Answer* Table::dealRiverCard()
{
    cards[4] = deck->deck[4];
    std::string str = "D:dealer=" + cards[4]->toString()
                                    + "=;";
    Request* req = new Request(str, getWaitingFor());
    Answer* answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, req);
    for (int i=0; i<tableSize; i++){
        if(players[i])
            answer->recievers.push_back(players[i]->regPlayer);
    }
    return answer;
}

Answer* Table::getPossibleActions()
{
    Answer* answer;
    unsigned int callSum = 0;
    if((*waitingFor)->stack == 0)
    {
        return new Answer(ANSWER_STATUS::FAILED, MSGTYPE::DEALER,
                          new Request("D:dealer:check==;", (*waitingFor)->regPlayer));
    }
    std::string actions = "D:dealer:action=";
    if((*lastRaiser)->moneyInPot == (*waitingFor)->moneyInPot)
    {
        actions += "X";
        if((*waitingFor)->stack > 0)
            actions +="R";
    }
    else
    {
        if((*waitingFor)->stack > 0)
            actions +="C";
        callSum = (*lastRaiser)->moneyInPot - (*waitingFor)->moneyInPot;
        if((*waitingFor)->stack > lastRaiseSum)
            actions +="R";
    }
    if(callSum)
        actions = actions + "=" + std::to_string(callSum) + ";";
    else
        actions +="=;";
    Request* req = new Request("D:dealer:fold==;", getWaitingFor());
    req->inMSG = actions;
    answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, req);
    answer->recievers.push_back(getWaitingFor());
    return answer;
}

void Table::newRound()
{
    std::cout << "New round..." << std::endl;
    if(pots.size() > 0)
    {
        Pot* cPot = *pots.rbegin();
        if(cPot->players.size() > 1)
        {
            waitingFor = ++((*(pots.rbegin()))->players.begin());
            lastRaiseSum = 0;
            lastRaiser = waitingFor;
        }
        else if (cPot->players.size() == 1)
        {
            std::cout << "one player in pot" << std::endl;
            lastRaiser = (*pots.rbegin())->players.begin();
            waitingFor = (*pots.rbegin())->players.begin();
        }
        else
        {
            lastRaiser = (*pots.rbegin())->players.end();
            waitingFor = (*pots.rbegin())->players.end();
            std::cout << "no players in pot" << std::endl;
        }
    }
    else
    {
        std::cout << "no pot" << std::endl;
    }
}

void Table::managePots()
{
    Pot* lastPot = *pots.rbegin();    
    if(lastPot->players.size()>2)
    {
        Pot* tmpPot;
        unsigned int minStack = (*lastPot->players.begin())->stack;
        std::vector <Player*> playersInPot = lastPot->players;
        Player* tmpPlayer;
        for(auto iter1 = playersInPot.begin();
                 iter1!= playersInPot.end();
                    ++iter1)
            for(auto iter2 = iter1;
                     iter2!= playersInPot.end();
                     ++iter2)
                if((*iter1)->moneyInPot > (*iter2)->moneyInPot)
                {
                    tmpPlayer = *iter1;
                    *iter1 = *iter2;
                    *iter2 = tmpPlayer;
                }
        lastPot->sum = (*playersInPot.begin())->moneyInPot * playersInPot.size();
        tmpPlayer = *playersInPot.begin();
        for(unsigned int i = 1; i<playersInPot.size(); i++)
        {
            if(tmpPlayer->moneyInPot < playersInPot[i]->moneyInPot)
            {
                tmpPlayer = playersInPot[i];
                tmpPot = new Pot();
                for(int j = i; j<playersInPot.size(); j++)
                    tmpPot->players.push_back(playersInPot[i]);
                tmpPot->sum = playersInPot[i]->moneyInPot * (playersInPot.size() - i);
                pots.push_back(tmpPot);
            }
        }
    }
}

unsigned int Table::getPotSum()
{
    return (*pots.rbegin())->sum;
}

std::vector <Answer*> Table::getDealingPlayers(Request* inReq)
{
    std::vector <Answer*> answers;
    for(unsigned int i=0; i<tableSize; i++)
    {
        if(players[i])
        {
            if(players[i]->pocketCards[0])
            {
                inReq->inMSG = "D:" + players[i]->regPlayer->getNickname()
                        + ":isdealt="+std::to_string(i) + "=;";
                Request* req = new Request(inReq->inMSG, inReq->owner);
                answers.push_back(new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, req));
            }
        }
    }
    return answers;
}

std::vector <RegisteredPlayers*> Table::getRecievers ()
{
    std::vector <RegisteredPlayers*> recievers;
    for (int i=0; i<tableSize; i++){
        if(players[i])
           recievers.push_back(players[i]->regPlayer);
    }
    return recievers;
}

Answer* Table::removeFromPots(RegisteredPlayers* rPlayer)
{
    if (pots.size() == 0)
        return nullptr;
    Answer* answer;
    if ((*waitingFor)->regPlayer == rPlayer)
    {
        std::string msg = "D:" + rPlayer->getNickname() + ":fold==;";
        Request* req = new Request(msg, rPlayer);
        answer = rPlayer->activeTable->deal(req);
        return answer;
    }
    Player* newLastRaiser;
    if((*lastRaiser)->regPlayer == rPlayer)
    {
        int sum = -1;
        Pot* pot = *pots.rbegin();
        for (auto iter = lastRaiser; iter!= pot->players.end(); ++iter)
        {
            Player* pl = *iter;
            if(iter != lastRaiser && pl->moneyInPot > sum)
            {
                newLastRaiser = pl;
                sum = newLastRaiser->moneyInPot;
            }
        }
        for(auto iter = pot->players.begin(); iter != lastRaiser; ++iter)
        {
            Player* pl = *iter;
            if(iter != lastRaiser && pl->moneyInPot > sum)
            {
                newLastRaiser = pl;
                sum = newLastRaiser->moneyInPot;
            }
        }
    }
    for(auto iter = pots.begin(); iter!= pots.end(); ++iter)
        for(auto iterP = (*iter)->players.begin();
                 iterP!= (*iter)->players.end(); ++iterP)
        {
            if((*iterP)->regPlayer == rPlayer)
            {
                (*iter)->players.erase(iterP);
                break;
            }
        }
    for (auto iter = (*pots.rbegin())->players.begin();
              iter!= (*pots.rbegin())->players.end();
                ++iter)
    {
        if(*iter == newLastRaiser)
        {
            lastRaiser = iter;
            break;
        }
    }
    return nullptr;
}

std::vector <Answer*> Table::finishDealt()
{
    std::vector <Answer*> Answers;
    unsigned int totalPot = 0;
    unsigned int diffPot = 0;
    for(auto iterPot = pots.begin();
             iterPot!= pots.end(); ++iterPot)
    {
        Pot* pot = (*iterPot);
        diffPot = pot->sum - totalPot;
        totalPot = pot->sum;
        std::vector <Player*> winners = whoWin(pot);
        for(auto iter = winners.begin();
                 iter!= winners.end(); ++iter)
        {
            Player* winner = *iter;
            winner->stack += diffPot/winners.size();
            winner->moneyInPot = 0;
            std::string reqMSG = "D:" + winner->regPlayer->getNickname()
                                + ":wins=" + std::to_string(diffPot/winners.size())
                                + "=;";
            Request* req = new Request(reqMSG, winner->regPlayer);
            Answer* answer = new Answer(ANSWER_STATUS::SUCCESS, MSGTYPE::DEALER, req);
            answer->recievers = getRecievers();
            Answers.push_back(answer);
        }
        //delete *iterPot;
    }
    for(std::vector<Pot*>::iterator iterPot = pots.begin(); iterPot != pots.end(); ++iterPot){
       delete *iterPot;
    }
    pots.clear();
    return Answers;
}

std::vector <Player*> Table::whoWin(Pot* pot)
{
    std::vector<Player*> winners;
    winners.push_back(*(pot->players.begin()));
    if (pot->players.size() == 1)
        return winners;
    for (auto iter = (++pot->players.begin());
              iter!= pot->players.end(); ++iter)
    {
        Player* comparable = *iter;
        int cmp = compare(*winners.begin(), comparable);
        if (cmp == 0)
            winners.push_back(comparable);
        else if (cmp == 2)
        {
            winners.clear();
            winners.push_back(comparable);
        }
    }
    return winners;
}

int Table::compare(Player* player1, Player* player2)
{
    int* hand1 = new int(7);
    int* hand2 = new int(7);
    std::string hand1Str = player1->pocketCards[0]->toString()
                          +player1->pocketCards[1]->toString()
                          +deck->deck[0]->toString()
                          +deck->deck[1]->toString()
                          +deck->deck[2]->toString()
                          +deck->deck[3]->toString()
                          +deck->deck[4]->toString();
    std::string hand2Str = player2->pocketCards[0]->toString()
                          +player2->pocketCards[1]->toString()
                          +deck->deck[0]->toString()
                          +deck->deck[1]->toString()
                          +deck->deck[2]->toString()
                          +deck->deck[3]->toString()
                          +deck->deck[4]->toString();
    char* hand1C = new char(15); //11
    char* hand2C = new char(15); //11
    for(int i=0; i<15; i++)
    {
        hand1C[i] = (char) hand1Str[i];
        hand2C[i] = (char) hand2Str[i];
    }
    hand1C[14] = 0;  //15
    hand2C[14] = 0;  //15
    ConvertFromPChar(hand1C, hand1);
    ConvertFromPChar(hand2C, hand2);
    return Compare (hand1, hand2);
}

void Table::cleanFromEmptyPlayers()
{
    std::cout << "cleaning from empty players" << std::endl;
    std::vector <Answer*> answers;
    for (unsigned int i =0; i < tableSize; i++)
        if(players[i])
            if(players[i]->stack < bigBlind)
            {
                std::string msg = "A:" + players[i]->regPlayer->getNickname() + ":leavesseat==;";
                Request* req = new Request(msg, players[i]->regPlayer);
                players[i]->regPlayer->leaveSeat(req);
            }
}
