#ifndef CARDS_H
#define CARDS_H

#include <string>
#include "exception.h"

class Cards
{
public:
    Cards(int);
    Cards(std::string);
    std::string toString();
    unsigned int getValue() {return value;}
protected:
    unsigned int orderNum;
    unsigned int value;
    unsigned int suit;
};

#endif // CARDS_H
