
#include "registeredplayers.h"

RegisteredPlayers::RegisteredPlayers (int sock, MultyServer *server)
{
    std::cout << "RegisteredPlayers constructor" << std::endl;
    loggedIn = false;
    activeTable = nullptr;
    nickname = "";
    sockfd = sock;
    freeMoney = 0;
    tableManager = tableManager->getInstance(server);
}

RegisteredPlayers::~RegisteredPlayers(){
    std::cout << "RegisteredPlayers deconstructor." << std::endl;
    if(activeTable)
    {
        std::string reqMSG = "A:" + this->getNickname() + ":leavesseat==;";
        activeTable->leaveSeat(new Request(reqMSG, this));
    }
}

Answer* RegisteredPlayers::logIn(Request *inRequest)
{
    //file format LOGIN=PASSWD 12546;
    Answer* answer = new Answer (ANSWER_STATUS::FAILED, MSGTYPE::ADMIN, inRequest);

    std::string login = "";
    std::string passwd = "";
    std::string buf = "";

    std::fstream passwds(filePath);
    if (!passwds.is_open()){
        std::cout << "File can't be opened" << std::endl;
    }
    std::string inLine;
    while (std::getline(passwds, inLine)){
        login = inLine.substr(0, inLine.find('='));
        if(login == inRequest->playerName)
            break;
        login.clear();
    }
    if (login.length() > 0){
        passwd = inLine.substr(inLine.find('=') + 1, inLine.find(' ') - inLine.find('=')-1);
        if(passwd.length() > 0 && passwd == inRequest->msg1){
            loggedIn = true;
            nickname = login;
            buf = inLine.substr(inLine.find(' ')+1, inLine.find(';') - inLine.find(' ') - 1);
            freeMoney = std::stoi(buf);
            answer->status = ANSWER_STATUS::SUCCESS;
        }
    }
    passwds.close();
    return answer;
}

Answer* RegisteredPlayers::takeSeat(Request * inRequest){
    if(activeTable == nullptr){
        BLINDS blinds;
        std::string tableName = inRequest->playerName;
        std::vector <Dealer *> *tables;

        if(std::stoi(tableName.substr(2, tableName.find('_')-2)) == 200)
            blinds = BLINDS::NL200;
        else if(std::stoi(tableName.substr(2, tableName.find('_')-2)) == 400)
            blinds = BLINDS::NL400;
        else if(std::stoi(tableName.substr(2, tableName.find('_')-2)) == 1000)
            blinds = BLINDS::NL1000;
        else
            return new Answer(ANSWER_STATUS::FAILED, MSGTYPE::ADMIN, inRequest);
        tables = tableManager->getTablesVector(blinds);
        for(auto iter = tables->begin(); iter != tables->end(); ++iter){
            if((*iter)->getName() == tableName){
                activeTable = *iter;
                return (*iter)->takeSeat(inRequest);
            }
        }
    }
    return new Answer(ANSWER_STATUS::FAILED, MSGTYPE::ADMIN, inRequest);
}

void RegisteredPlayers::leaveSeat(Request * inRequest){
    if(activeTable)
        if(activeTable->leaveSeat(inRequest))
            activeTable = nullptr;
}
