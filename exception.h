#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <string>

class Exception
{
public:
    Exception(std::string msg){
        message = msg;
    }
    std::string message;
};

#endif // EXCEPTION_H
