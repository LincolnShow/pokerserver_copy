#ifndef REQUEST_H
#define REQUEST_H

#include <string>
#include <iostream>
#include <constants.h>

#include "exception.h"
#include "registeredplayers.h"

class RegisteredPlayers;

class Request
{
public:
    Request(std::string, RegisteredPlayers *);
    Request(){}

    RegisteredPlayers * owner;

    MSGTYPE type;
    std::string playerName;

    ACTIONS action;
    unsigned int value1;
    unsigned int value2;

    std::string msg1, msg2;

    std::string textMessage;

    std::string inMSG;
};

#endif // REQUEST_H
