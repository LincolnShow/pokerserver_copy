#include "tablemanager.h"

TableManager* TableManager::itself = nullptr;

TableManager::TableManager (MultyServer* server)
{
    std::vector<Dealer*> limitVector;
    limitVector.push_back(new Dealer(server, BLINDS::NL200, "NL200_1"));
    std::pair <BLINDS, std::vector <Dealer*> > pair;
    pair.first = BLINDS::NL200;
    pair.second = limitVector;
    Tables.insert(pair);

    limitVector.clear();
    limitVector.push_back(new Dealer(server, BLINDS::NL400, "NL400_1"));
    pair.first = BLINDS::NL400;
    pair.second = limitVector;
    Tables.insert(pair);

    limitVector.clear();
    limitVector.push_back(new Dealer(server, BLINDS::NL1000, "NL1000_1"));
    pair.first = BLINDS::NL1000;
    pair.second = limitVector;
    Tables.insert(pair);
}

void TableManager::addNewTable(BLINDS blinds){
    std::pair <BLINDS, std::vector <Dealer*> > pair;
    pair = *(Tables.find(blinds));
    Dealer* lastTable = *(pair.second.rbegin());
}

std::vector <Dealer*>* TableManager::getTablesVector (BLINDS blinds){
    return &((*Tables.find(blinds)).second);
}

unsigned int TableManager::getNumOfTables()
{
    unsigned int N=0;
    for(auto iter = Tables.begin(); iter != Tables.end(); ++iter){
        N += (*iter).second.size();
    }
    return N;
}

std::map<std::string, unsigned int> TableManager::getTableNames()
{
    std::map<std::string, unsigned int> tables;
    for(auto iter = Tables.begin(); iter != Tables.end(); ++iter)
        for(auto iter2 = (*iter).second.begin();
                 iter2!= (*iter).second.end(); ++iter2){
            std::pair<std::string, unsigned int> table;
            table.first = (*iter2)->getName();
            table.second = (*iter2)->getTotalPlayers();
            tables.insert(table);
        }
    return tables;
}

Dealer* TableManager::getTable(std::string tableName)
{
    std::map<std::string, unsigned int> players;
    for(auto iter = Tables.begin(); iter != Tables.end(); ++iter)
        for(auto iter2 = (*iter).second.begin();
                 iter2!= (*iter).second.end(); ++iter2){
            if((*iter2)->getName() == tableName){
                return (*iter2);
            }
        }
}
