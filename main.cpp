#include <iostream>
#include <assert.h>
#include <fstream>

#include "cards.h"
#include "request.h"
#include "exception.h"
#include "dealer.h"
#include "multyserver.h"

using namespace std;
/*
class CardTest
{
public:
    bool go()
    {
        int testCardInt = 50;
        try {
            Cards * newCard = new Cards(testCardInt);
            assert (newCard->orderNum == testCardInt);
            assert (newCard->value == testCardInt / 4);
            assert (newCard->suit == testCardInt % 4);
        }
        catch (Exception Error)
        {
            cout << Error.message << endl;
            exit (1);
        }
        try {
            string card = "Ah";
            Cards * newCard = new Cards (card);
            assert (newCard->orderNum == 48);
        }
        catch (Exception Error)
        {
            cout << Error.message << endl;
        }
        try {
            string card = "Kd";
            Cards * newCard = new Cards (card);
            assert (newCard->orderNum == 45);
        }
        catch (Exception Error)
        {
            cout << Error.message << endl;
        }
        try {
            string card = "2s";
            Cards * newCard = new Cards (card);
            assert (newCard->orderNum == 3);
        }
        catch (Exception Error)
        {
            cout << Error.message << endl;
        }
        return true;
    }
};

class RequestTest
{
public:
    RequestTest() {}
    bool go()
    {
        string testMsg = "D:Dzhei:takesseat=3=80;";
        try {
            Request * r = new Request(testMsg);
            assert (r->action == ACTIONS::TAKES_SEAT);
            assert (r->value1 == 3);
            assert (r->type == MSGTYPE::DEALER);
            assert (r->playerName == "Dzhei");
            assert (r->value2 == 80);
        }
        catch (Exception error){
            cout << error.message << endl;
        }

        testMsg = "D:Dealer:flop=AhJdTs=;";
        try {
            Request * r = new Request(testMsg);
            assert (r->action == ACTIONS::DEALINGFLOP);
            assert (r->msg1 == "AhJdTs");
            assert (r->type == MSGTYPE::DEALER);
            assert (r->playerName == "Dealer");
        }
        catch (Exception error){
            cout << error.message << endl;
        }

        testMsg = "D:Dzhei:raise=20=;";
        try {
            Request * r = new Request(testMsg);
            assert (r->action == ACTIONS::RAISE);
            assert (r->msg1 == "20");
            assert (r->type == MSGTYPE::DEALER);
            assert (r->value1 == 20);
        }
        catch (Exception error){
            cout << error.message << endl;
        }

        testMsg = "D:Dzhei:call==;";
        try {
            Request * r = new Request(testMsg);
            assert (r->action == ACTIONS::CALL);
            assert (r->type == MSGTYPE::DEALER);
        }
        catch (Exception error){
            cout << error.message << endl;
        }

        testMsg = "C:Dzhei:some message==;";
        try {
            Request * r = new Request(testMsg);
            assert (r->action == ACTIONS::NONE);
            assert (r->type == MSGTYPE::CHAT);
            assert (r->textMessage == string("some message"));
        }
        catch (Exception error){
            cout << error.message << endl;
        }
    }
};

class DealerTest
{
public:
    void go(std::string fileName){
        ifstream testFile;
        testFile.open(fileName);
        if(!testFile.is_open()){
            std::cout << "No test file!" << endl;
        }
        else {
            string inString;
            Dealer testDealer(BLINDS::NL1000, "test table");
            while(1){

            }
            testFile.close();
        }
    }
};
*/
int main(int argc, char *argv[])
{
//    CardTest cardTest;
//    cardTest.go();

//    RequestTest rTest;
//    rTest.go();
    MultyServer* server = new MultyServer(3333);
    return 0;
}
