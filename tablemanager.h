#ifndef TABLEMANAGER_H
#define TABLEMANAGER_H

#include <vector>
#include <map>

#include "dealer.h"
#include "constants.h"
#include "exception.h"
#include "multyserver.h"

class Answer;
class Dealer;
class MultyServer;

class TableManager
{
public:
    static TableManager* getInstance (MultyServer* server)
    {
        if (itself == nullptr){
            itself = new TableManager(server);
        }
        return itself;
    }

//    Answer* takeSeat(Request*){}
    std::vector <Dealer*>* getTablesVector (BLINDS);
    unsigned int getNumOfTables();
    std::map<std::__cxx11::string, unsigned int> getTableNames();
    Dealer* getTable(std::string);

private:
    static TableManager* itself;
    TableManager(MultyServer* server);
    TableManager(){}

    std::map <BLINDS, std::vector <Dealer*> > Tables;
    void addNewTable(BLINDS);
};

#endif // TABLEMANAGER_H
