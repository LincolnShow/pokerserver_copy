#include "cards.h"

Cards::Cards(int num)
{
    if (num<0||num>51)
        throw Exception(std::string ("Card num is out of range"));
    else
    {
        orderNum = num;
        value = num / 4;
        suit = num % 4;
    }
}

Cards::Cards(std::string card)
{
    int cardOrder = 0;
    switch (card[0]) {
    case 'A':
        cardOrder = 48;
        break;
    case 'K':
        cardOrder = 44;
        break;
    case 'Q':
        cardOrder = 40;
        break;
    case 'J':
        cardOrder = 36;
        break;
    case 'T':
        cardOrder = 32;
        break;
    case '9':
        cardOrder = 28;
        break;
    case '8':
        cardOrder = 24;
        break;
    case '7':
        cardOrder = 20;
        break;
    case '6':
        cardOrder = 16;
        break;
    case '5':
        cardOrder = 12;
        break;
    case '4':
        cardOrder = 8;
        break;
    case '3':
        cardOrder = 4;
        break;
    case '2':
        cardOrder = 0;
        break;
    default:
        throw(Exception(std::string ("cardValue: Wrong in string")));
        break;
    }
    switch (card[1]) {
    case 'h':
        break;
    case 'd':
        cardOrder += 1;
        break;
    case 'c':
        cardOrder += 2;
        break;
    case 's':
        cardOrder += 3;
        break;
    default:
        throw(Exception(std::string ("cardSuit: Wrong in string")));
        break;
    }
    orderNum = cardOrder;
    value = cardOrder / 4;
    suit = cardOrder % 4;
}

std::string Cards::toString()
{
    std::string outString;
    switch (value) {
    case 12:
        outString = "A";
        break;
    case 11:
        outString = "K";
        break;
    case 10:
        outString = "Q";
        break;
    case 9:
        outString = "J";
        break;
    case 8:
        outString = "T";
        break;
    case 7:
        outString = "9";
        break;
    case 6:
        outString = "8";
        break;
    case 5:
        outString = "7";
        break;
    case 4:
        outString = "6";
        break;
    case 3:
        outString = "5";
        break;
    case 2:
        outString = "4";
        break;
    case 1:
        outString = "3";
        break;
    case 0:
        outString = "2";
        break;
    default:
        break;
    }
    switch (suit) {
    case 0:
        outString += "h";
        break;
    case 1:
        outString += "d";
        break;
    case 2:
        outString += "c";
        break;
    case 3:
        outString += "s";
        break;
    default:
        break;
    }
    return outString;
}
